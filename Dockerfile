FROM ubuntu

WORKDIR /app
COPY apt-requirements.txt ./

RUN apt-get update && apt-get install -y --no-install-recommends \
        $(cat apt-requirements.txt) \
        dumb-init \
&& rm -rf /var/lib/apt/lists/*

COPY setup.py setup.cfg ./
RUN python3 -m pip install -e .

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
