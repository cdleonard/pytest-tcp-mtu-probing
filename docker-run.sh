#! /bin/bash

set -e

if [[ $1 == -h || $1 == --help ]]; then
    cat >&2 <<MSG
docker-run.sh
=============

Build docker container, mount source inside it and run a command

Options
-------
MSG
    exit 2
fi

base_path=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)

# format docker build cmd
docker_build_cmd=(docker build "$base_path")

"${docker_build_cmd[@]}"
image_id=$("${docker_build_cmd[@]}" -q)
docker_run_cmd=(
    docker run \
    --interactive \
    --tty \
    --privileged \
    --volume "$base_path:/app" \
    --workdir /app
)
docker_run_cmd+=("$image_id")

exec "${docker_run_cmd[@]}" "$@"
